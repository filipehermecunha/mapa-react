import styled from 'styled-components' 


export const Molde = styled.div`

    display: flex;
    justify-content: center;
    align-content: center;
    width: 22rem;
    height: 12rem;
    border-radius: 0.625rem;
    box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
    background-color: white;
    align-items: center;
    flex-wrap: wrap;
    z-index: 1;
    position: absolute;
    margin: 0.4rem;

`
export const InputBox = styled.input`
    width: 18rem;
    height: 2.19rem;
    border-color: black;
    border-style: solid;
    border-radius: 0.375rem;
    border-width: 0.1rem;
    color: #757575;
    padding-left: 1rem ;
    font-size: 1rem;
    font-style: normal;
    font-weight: 400;
`

export const InputLabel = styled.label`
    color: black ;

`

export const InputModel = styled.div`
    display: inline-flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 0.5rem;
    padding-bottom: 0.7rem;


`