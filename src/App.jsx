import logo from './logo.svg';
import './App.css';
import MapComponent from './mapComponent';
import { useState } from 'react';

function App() {
  return (
    <div className="App">
     <MapComponent />
    </div>
  );
}

export default App;
