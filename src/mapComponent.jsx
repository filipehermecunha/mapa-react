import React, { useEffect, useRef, useState } from "react";
import "ol/ol.css";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";

import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import LineString from "ol/geom/LineString.js";
import Style from "ol/style/Style.js";
import Icon from "ol/style/Icon.js";
import Stroke from "ol/style/Stroke.js";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { fromLonLat } from "ol/proj";
import Polyline from "ol/format/Polyline.js";
import * as olProj from "ol/proj";
import Layer from "ol/layer/Layer.js";
import { InputBox, InputLabel, InputModel, Molde } from "./styled";
import Overlay from 'ol/Overlay.js';
import {OGCMapTile} from 'ol/source.js';

import axios from "axios"

const apiKey = "5b3ce3597851110001cf62484b8aefd3291842cbba9a61fcd1c46367";
var originCoord = "";
let destinationCoord = "";

const cordOriginArray = []
const cordDestArray = []

const MapComponent = () => {
  const [originQuery, setOriginQuery] = useState("");
  const [originResults, setOriginResults] = useState([]);
  
  const [destinationQuery, setDestinationQuery] = useState("");
  const [destinationResults, setDestinationResults] = useState([]);
  
  const [directionsResults, setDirections] = useState();

  const originDados = originResults.map((result) => (
    {
      id: result.properties.id,
      name: result.properties.name,
      coordinates: result.geometry.coordinates,
      region: result.properties.region_a,
    }
    
  ));

  const destinationDados = destinationResults.map((result) => (
    {
      id: result.properties.id,
      name: result.properties.name,
      coordinates: result.geometry.coordinates,
      region: result.properties.region_a,
    }
    
  ));

  for (const elemento of originDados) {
    if (elemento.name +", " + elemento.region === originQuery) {
      cordOriginArray.push(elemento.coordinates)
    }
  }
  
  for (const elemento of destinationDados) {
    if (elemento.name +", " + elemento.region === destinationQuery) {
      cordDestArray.push(elemento.coordinates)
    }
  }

  const originCoordenadas = cordOriginArray[0]
  const destinationCoordenadas = cordDestArray[0]

  const latOrigin = originCoordenadas?.[0]
  const longOrigin = originCoordenadas?.[1]

  const latDest = destinationCoordenadas?.[0]
  const longDest = destinationCoordenadas?.[1]

  originCoord = latOrigin +","+ longOrigin
  destinationCoord = latDest +","+ longDest

  const handleInputChange = async (e, setQuery, setResults) => {
    const inputValue = e.target.value;
    setQuery(inputValue);

    try {
      const response = await fetch(
        `https://api.openrouteservice.org/geocode/autocomplete?api_key=${apiKey}&text=${inputValue}`
      );

      if (!response.ok) {
        throw new Error("Erro ao buscar resultados de geocodificação");
      }

      const data = await response.json();
      if (data.features) {
        setResults(data.features);
      } else {
        setResults([]);
      }
    } catch (error) {
      console.error(error);
    }
   
  };  
 
  const handleDirections = async () => {
 
        try {
          const response = await fetch(
            `https://api.openrouteservice.org/v2/directions/driving-car?api_key=${apiKey}&start=${originCoord}&end=${destinationCoord}`
          )
  
          if (!response.ok) {
            throw new Error("Erro ao obter direções");
          }
  
          const data = await response.json();
  
          const polylineData = data.features[0].geometry.coordinates;
          setDirections(polylineData);
        }
        catch (error) {
          console.error("Erro ao obter direções:", error);
        }

  };

  const polyline = directionsResults;
 
  const mapRef = useRef(null);

  useEffect(() => {
    if (!mapRef.current) {
      mapRef.current = new Map({
        target: "map",
        layers: [
          new TileLayer({
            source: new OSM(),
          }),
        ],
        view: new View({
          center: [-4842800.131497843, -2318792.1804284034],
          zoom: 15,
        }),
      });
    }
  }, []);

  const latlngs = [];
  const routeArr = [];

  const routeArray = [];

  if (latlngs) {
    polyline?.map(function (coord) {
      routeArr.push.apply(routeArr, olProj.fromLonLat(coord));
    });

    for (let i = 0; i < routeArr.length; i += 2) {
      routeArray.push(routeArr.slice(i, i + 2));
    }
  }

  var sourceVector = new VectorSource({
    features: [new Feature(new LineString(routeArray))],
  });

  var path = new VectorLayer({
    source: sourceVector,
    style: new Style({
      stroke: new Stroke({
        color: "red",
        width: 4,
        weight: 2,
      }),
    }),
    className: "Line"
  });
  
  useEffect(() => {
    if (originCoord !== null && destinationCoord !== null){
      handleDirections();
    } 
  }, [originCoord, destinationCoord]);

  try {
    mapRef.current.addLayer(path);
    
  } catch (error) {
    console.error('Erro:', error);
  }
  
  
  return (
    <div id="map" style={{ width: "100%", height: "100vh" }}>
      <Molde>
        <InputModel>
          <InputLabel>Origem:</InputLabel>
          <InputBox
            type="text"
            placeholder="Digite o local de origem"
            value={originQuery}
            onChange={(e) =>
              handleInputChange(e, setOriginQuery, setOriginResults)
            }
            list="originResults"
          />
        </InputModel>

        <datalist id="originResults">
          {originResults.map((result) => (
            <option
              key={result.properties.id}
              value={`${
                result.properties.name
              }, ${
                result.properties.region_a
              }`}
            />
          ))}
        </datalist>

        <InputModel>
          <InputLabel>Destino:</InputLabel>
          <InputBox
            placeholder="Digite o local de destino"
            type="text"
            value={destinationQuery}
            onChange={(e) =>
              handleInputChange(e, setDestinationQuery, setDestinationResults)
            }
            list="destinationResults"
          />
        </InputModel>

        <datalist id="destinationResults">
          {destinationResults.map((result) => (
            <option
              key={result.properties.id}
              value={`${
                result.properties.name
              }, ${
                result.properties.region_a
              }`}
            />
          ))}
        </datalist>
      </Molde>
    </div>
  );
};

export default MapComponent;