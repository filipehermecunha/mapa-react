
import Style from "ol/style/Style"
import Stroke from "ol/style/Stroke"
import { fromLonLat } from "ol/proj"
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import { LineString } from 'ol/geom';
import { Map } from 'ol'

// interface ICreateRouteProps {
//     map: Map,
//     originCoordinates: [],
//     destinationCoordinates: [],
//     apiKey: string
// }
